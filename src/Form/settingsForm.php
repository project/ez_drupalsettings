<?php
/**  
 * @file  
 * Contains Drupal\ez_drupalsettings\Form\settingsForm.  
 */  

namespace Drupal\ez_drupalsettings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class settingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ez_drupalsettings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('ez_drupalsettings.settings');
    $form['keys_and_values'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additions to drupalSettings'),
      '#required' => FALSE,
      '#description' => $this->t("One drupalSettings addition per line. It must define a key, a pipe character, and a value, e.g.: <em>key|value</em><br />Accepts tokens and supports the <a href='https://www.drupal.org/project/token' target='_blank'>Token</a> module if installed."),
      '#default_value' => $config->get('keys_and_values'),
    ];

    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('token')) {
      $form['tokens'] = \Drupal::service('token.tree_builder')->buildRenderable(['node', 'user']);
      // Pass your entity machine name as parameter in an array to add it to the list.
    }
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $settings = $form_state->getValue('keys_and_values');
    if ($settings) {
      $settings = explode("\n", $settings);
      foreach ($settings as $line) {
        if (!preg_match("/^\S+\|\S+/", $line) && !empty($line)) {
          $form_state->setErrorByName(
            'keys_and_values',
            $this->t('Line "@line" is not properly formed. Correct format is a key, a pipe character, and a value, e.g.: <em>key|value</em>.', ['@line' => $line])
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ez_drupalsettings.settings');
    $config->set('keys_and_values', trim($form_state->getValue('keys_and_values')));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ez_drupalsettings.settings',
    ];
  }

}